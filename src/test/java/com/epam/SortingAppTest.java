package com.epam;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SortingAppTest {
    private final String[] input;
    private final String expectedOutput;

    public SortingAppTest(String[] input, String expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }

    @Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                // No arguments
                {new String[]{}, "No arguments provided.\n"},
                // One argument
                {new String[]{"5"}, "Sorted numbers: [5]\n"},
                // Ten arguments
                {new String[]{"5", "3", "8", "1", "9", "2", "6", "7", "4", "0"}, "Sorted numbers: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]\n"},
                // More than ten arguments
                {new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"}, "Too many arguments. Provide up to 10 integers.\n"},
                // Invalid input
                {new String[]{"a", "b"}, "Invalid input. Please provide integer values.\n"},
                // Boundary cases for empty input, invalid input, one element, ten elements
                {new String[]{"0"}, "Sorted numbers: [0]\n"},
                {new String[]{"-100"}, "Sorted numbers: [-100]\n"},
                {new String[]{"11"}, "Sorted numbers: [11]\n"}
        });
    }

    @Test
    public void testSortingApp() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        SortingApp.main(input);
        String actualOutput = outContent.toString().replace("\r\n", "\n");
        String normalizedExpectedOutput = expectedOutput.replace("\r\n", "\n");
        assertEquals(normalizedExpectedOutput, actualOutput);
    }
}
