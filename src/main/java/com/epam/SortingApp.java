package com.epam;

import java.util.Arrays;


public class SortingApp {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("No arguments provided.");
            return;
        }

        if (args.length > 10) {
            System.out.println("Too many arguments. Provide up to 10 integers.");
            return;
        }

        try {
            int[] numbers = new int[args.length];
            for (int i = 0; i < args.length; i++) {
                numbers[i] = Integer.parseInt(args[i]);
            }
            Arrays.sort(numbers);
            System.out.println("Sorted numbers: " + Arrays.toString(numbers));
        } catch (NumberFormatException e) {
            System.out.println("Invalid input. Please provide integer values.");
        }
    }
}
